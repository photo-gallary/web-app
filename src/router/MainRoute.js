import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import LoginPage from "../screens/LoginPage";
import HomePage from "../screens/HomePage";
import CreateGallaryPage from "../screens/CreateGallaryPage";
import Gallary from "../screens/Gallary";
import SignUpPage from "../screens/SignUpPage";
import { useSelector } from "react-redux";
import EditImagePage from "../screens/EditImagePage";
import CallbackFacebook from "../screens/CallbackFacebook";

export default function App() {
  const isLogin = useSelector((state) => state.auth.isLogin);
  const isLogin2 = localStorage.getItem("isLogin");
  const token = localStorage.getItem("token");

  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <HomePage />
        </Route>
        <Route exact path="/gallaries">
          <HomePage />
        </Route>
        <Route exact path="/signin">
          <LoginPage />
        </Route>
        <Route exact path="/signup">
          <SignUpPage />
        </Route>
        <Route exact path="/gallary/:id">
          <Gallary />
        </Route>
        <Route exact path="/callback">
          <CallbackFacebook />
        </Route>
        {isLogin2 === "true" && token !== "null" ? (
          <>
            <Route exact path="/gallaries/create">
              <CreateGallaryPage />
            </Route>
            <Route exact path="/gallaries/edit/:id">
              <EditImagePage />
            </Route>
          </>
        ) : (
          <Redirect to="/signin" />
        )}
      </Switch>
    </Router>
  );
}
