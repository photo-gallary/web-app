import { LOGIN, LOGOUT } from "../action/AuthAction";

const AUTH_STATE = {
  isLogin: false,
  token: undefined,
  error: "",
};

export const AuthReducer = (state = AUTH_STATE, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        token: action.payLoad,
        isLogin: true,
      };
    case LOGOUT:
      return AUTH_STATE;
    default:
      return state;
  }
};
