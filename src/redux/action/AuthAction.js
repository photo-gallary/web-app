export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";

export const loginSuccess = (token) => ({
  type: LOGIN,
  payLoad: token,
});

export const logout = () => ({
  type: LOGOUT,
});
