import React, { useState, useEffect } from "react";
import Header from "../components/Header";
import {
  Card,
  Typography,
  Grid,
  CardHeader,
  CardMedia,
  IconButton,
  CardActions,
  CardContent,
  InputBase,
} from "@material-ui/core";
import {
  fetchGallary,
  getSession,
  fetchOpenGallary,
  getUserById,
} from "../api/index";
import { fade, makeStyles } from "@material-ui/core/styles";
import SearchIcon from "@material-ui/icons/Search";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import ShowCard from "../components/ShowCard";
import LoadingCard from "../components/LoadingCard";
const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
    minWidth: 230,
    minHeight: 330,
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  container: {
    backgroungColor: "red",
    padding: "24px 112px 24px 112px",
    height: "72opx",
    textAlign: "center",
  },
  search: {
    position: "relative",
    // right: 24,
    // top: 120,
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.black, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.black, 0.25),
    },
    marginLeft: 24,
    width: "100%",
    [theme.breakpoints.up("md")]: {
      marginLeft: "calc(40% - 24px)",
      width: "20%",
    },
    marginBottom: 56,
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  inputRoot: {
    color: "inherit",
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: "12ch",
      "&:focus": {
        width: "20ch",
      },
    },
  },
}));

const Homepage = () => {
  const classes = useStyles();
  const [gallaryList, setgallaryList] = useState([]);
  const token = useSelector((state) => state.auth.token);
  const isLogin = localStorage.getItem("isLogin");
  const [keyword, setkeyword] = useState("");
  const [loading, setloading] = useState(false);
  useEffect(() => {
    fetchList();
  }, []);

  const fetchList = () => {
    if (isLogin === "true") {
      setloading(true);
      getSession(token).then((res) => {
        fetchGallary(token, res.data.ID)
          .then((resp) => {
            let sorted = resp.data.sort(
              (a, b) => new Date(b.CreatedAt) - new Date(a.CreatedAt)
            );
            setgallaryList(sorted);
            setloading(false);
          })
          .catch((err) => {
            console.log("err", err);
          });
      });
    } else {
      setloading(true);
      fetchOpenGallary().then((res) => {
        let sorted = res.data.sort(
          (a, b) => new Date(b.CreatedAt) - new Date(a.CreatedAt)
        );
        setgallaryList(sorted);
        setloading(false);
      });
    }
  };

  const searchByKeyword = (event) => {
    setkeyword(event.target.value);
  };
  console.log("gallaryList", gallaryList);
  console.log("gallaryList.length", gallaryList.length);
  return (
    <div>
      <Header title="Photo Gallary" />
      <div className={classes.container}>
        <div className={classes.search}>
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <InputBase
            placeholder="Search…"
            value={keyword}
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            inputProps={{ "aria-label": "search" }}
            onChange={(e) => searchByKeyword(e)}
          />
        </div>
        {gallaryList.length === 0 ? (
          <Typography style={{ marginTop: 360 }}>No gallary</Typography>
        ) : (
          <Grid container spacing={3}>
            {gallaryList
              .filter((data) => {
                if (keyword == "") return data;
                else if (
                  data.name.toLowerCase().includes(keyword.toLowerCase())
                ) {
                  return data;
                }
              })
              .map((item, index) => {
                return item.length === 0 ? (
                  <Typography style={{ marginTop: 360 }}>
                    No gallary found
                  </Typography>
                ) : item.Status ? (
                  loading ? (
                    <LoadingCard loading={true} />
                  ) : (
                    <>
                      <ShowCard item={item} index={index} shareButton={false} />
                    </>
                  )
                ) : (
                  <> </>
                );
              })}
          </Grid>
        )}
      </div>
    </div>
  );
};

export default Homepage;
