import React, { useEffect, useState } from "react";
import Header from "../components/Header";
import { useParams } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import {
  Card,
  CardContent,
  Typography,
  Grid,
  TextField,
  Button,
  CardHeader,
  IconButton,
  Collapse,
  Tooltip,
} from "@material-ui/core";
import { useSelector } from "react-redux";
import { fetchImages, addImage, deleteImage } from "../api/index";
import ImageCard from "../components/ImageCard";
import ClearIcon from "@material-ui/icons/Clear";
import { Alert, AlertTitle } from "@material-ui/lab";
import AddPhotoAlternateIcon from "@material-ui/icons/AddPhotoAlternate";
const useStyles = makeStyles((theme) => ({
  root: {
    // minWidth: 230,
    minHeight: 320,
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  avatar: {
    backgroundColor: "red",
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(6, 36, 8),
    outline: "unset",
  },
  button: {
    height: 56,
    width: "100%",
  },
}));

const EditImagePage = () => {
  let { id } = useParams();
  const classes = useStyles();
  const [imgList, setimgList] = useState([]);
  const [caption, setcaption] = useState("");
  const [file, setfile] = useState(null);
  const [showImage, setshowImage] = useState(null);
  const [hover, sethover] = useState(false);
  const token = useSelector((state) => state.auth.token);
  const [success, setsuccess] = useState(false);
  const [delSuccess, setdelSuccess] = useState(false);
  console.log("file", file);
  useEffect(() => {
    fetchImg();
  }, []);

  const handleAdd = () => {
    const formData = new FormData();
    if (file === null) {
      return;
    }
    formData.append("photo", inputFile.current.files[0]);
    formData.append("caption", caption || "Photo");
    addImage(token, id, formData)
      .then(() => {
        fetchImg();
        setsuccess(true);
        setTimeout(() => {
          setsuccess(false);
        }, 5000);
      })
      .catch((err) => {
        console.log("err", err);
      });
    setcaption("");
    setfile(null);
    setshowImage(null);
  };

  const handleDelete = (item) => {
    deleteImage(token, item.ID)
      .then(() => {
        fetchImg();
        setdelSuccess(true);
        setTimeout(() => {
          setdelSuccess(false);
        }, 5000);
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  const fetchImg = () => {
    fetchImages(token, id)
      .then((res) => {
        console.log("res", res);
        const sorted = res.data.sort(
          (a, b) => new Date(b.CreatedAt) - new Date(a.CreatedAt)
        );
        console.log("sorted", sorted);
        setimgList(sorted);
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  const inputFile = React.useRef(null);

  const showOpenFileDlg = () => {
    inputFile.current.click();
  };

  const handleFileChange = (event) => {
    // URL.createObjectURL(
    setshowImage(URL.createObjectURL(event.target.files[0]));
    setfile(event.target.files[0]);
  };

  return (
    <div>
      <Header title="Edit Gallary" />
      <div style={styles.container}>
        <Grid container spacing={3}>
          <Grid item md={3}>
            <Card
              className={classes.root}
              style={styles.addCard}
              //   onClick={handleOpen}
            >
              <CardHeader
                action={
                  <Tooltip title="Clear" placement="top">
                    <IconButton
                      aria-label="delete image"
                      onClick={() => {
                        setcaption("");
                        setfile(null);
                        setshowImage(null);
                      }}
                    >
                      <ClearIcon />
                    </IconButton>
                  </Tooltip>
                }
                style={{ color: "#000" }}
              />
              <CardContent>
                {file !== null ? (
                  <img src={showImage} style={styles.uploadImg} />
                ) : (
                  <div
                    style={hover ? styles.hoverTextBG : styles.normalTextBg}
                    onMouseOver={() => sethover(true)}
                    onMouseLeave={() => sethover(false)}
                    onClick={showOpenFileDlg}
                  >
                    <AddPhotoAlternateIcon
                      style={{ fontSize: 56, marginLeft: "calc(50% - 28px)" }}
                    />
                    <Typography
                      style={hover ? styles.hoverText : styles.normalText}
                    >
                      New Image
                    </Typography>
                  </div>
                )}

                <input
                  type="file"
                  id="file"
                  ref={inputFile}
                  onChange={handleFileChange}
                  style={{ display: "none" }}
                />
                <Grid
                  container
                  spacing={2}
                  style={{ justifyContent: "center" }}
                >
                  <Grid md={9}>
                    <TextField
                      id="outlined-basic"
                      label="Caption"
                      variant="outlined"
                      style={{ width: "100%" }}
                      value={caption}
                      onChange={(e) => setcaption(e.target.value)}
                    />
                  </Grid>
                  <Grid md={3}>
                    <Button
                      variant="outlined"
                      color="primary"
                      className={classes.button}
                      onClick={handleAdd}
                    >
                      Add
                    </Button>
                  </Grid>
                </Grid>

                {/* <Button onClick={() => {}}>
                  Upload
                </Button> */}
              </CardContent>
            </Card>
          </Grid>
          {imgList.map((item, index) => {
            return (
              <ImageCard
                key={index}
                item={item}
                index={index}
                token={token}
                onDeleteClick={() => {
                  handleDelete(item);
                }}
                onClick={() => {
                  alert("edit");
                }}
                onFavClick={() => {
                  alert("fav");
                }}
                onShareClick={() => {
                  alert("Share");
                }}
                shareButton={false}
                favButton={false}
                settingButton={false}
                deleteButton={false}
                deleteMode={true}
                path={true}
              />
            );
          })}
        </Grid>
      </div>

      {success ? (
        <Collapse in={success}>
          <Alert
            severity="success"
            style={{
              position: "fixed",
              bottom: 12,
              right: 12,
              left: "70%",
              backgroundColor: "#333",
              color: "#fff",
            }}
          >
            <AlertTitle>Success</AlertTitle>
            Successfully upload new image — <strong>check it out!</strong>
          </Alert>
        </Collapse>
      ) : (
        <> </>
      )}
      {delSuccess ? (
        <Collapse in={delSuccess}>
          <Alert
            severity="error"
            style={{
              position: "fixed",
              bottom: 12,
              right: 12,
              left: "70%",
              backgroundColor: "#333",
              color: "#fff",
            }}
          >
            <AlertTitle>Deleted</AlertTitle>
            Successfully deleted image — <strong>check it out!</strong>
          </Alert>
        </Collapse>
      ) : (
        <> </>
      )}
    </div>
  );
};

export default EditImagePage;

const styles = {
  container: {
    flex: 1,
    padding: "24px 56px",
  },
  addCard: {
    border: "2px dashed black",
    cursor: "pointer",
  },
  addedCard: {
    // height: 240,
  },
  gallaryText: { textAlign: "center", fontSize: 24, fontWeight: "bold" },
  normalTextBg: {
    transition: "ease 0.3s",
    minHeight: 230,
    opacity: 0.6,
    paddingTop: 170,
    paddingBottom: 50,
    marginBottom: 48,
  },
  hoverTextBG: {
    transition: "ease 0.3s",
    paddingTop: 170,
    paddingBottom: 50,
    minHeight: 230,
    backgroundColor: "#eee",
    marginBottom: 48,
    borderRadius: 12,
  },
  normalText: {
    textAlign: "center",
    fontSize: 24,
    fontWeight: "bold",
  },
  hoverText: {
    textAlign: "center",
    fontSize: 24,
    fontWeight: "bold",
    color: "#333",
  },
  uploadImg: {
    marginBottom: 48,
    width: "100%",
    padding: 0,
  },
  button: {
    width: "100%",
  },
};
