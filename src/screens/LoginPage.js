import React, { useState } from "react";
import {
  Collapse,
  Avatar,
  Button,
  CssBaseline,
  TextField,
  FormControlLabel,
  Checkbox,
  Link,
  Paper,
  Box,
  Grid,
  Typography,
} from "@material-ui/core";

import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import FacebookIcon from "@material-ui/icons/Facebook";
import { makeStyles } from "@material-ui/core/styles";
import { signin, signinGoogle, host } from "../api/index";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { loginSuccess } from "../redux/action/AuthAction";
import { fbauthorizeURL, fbappID, fbredirectUrl } from "../api/oauth";
import { Alert, AlertTitle } from "@material-ui/lab";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        My Gallary
      </Link>
      {new Date().getFullYear()}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
  },
  image: {
    backgroundImage: "url(https://source.unsplash.com/random)",
    backgroundRepeat: "no-repeat",
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[50]
        : theme.palette.grey[900],
    backgroundSize: "cover",
    backgroundPosition: "center",

    // marginLeft: "auto",
    // marginRight: "auto",
  },
  paper: {
    margin: theme.spacing(2, 8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(1, 0, 2),
  },
}));

export default function LoginPage() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();

  const [email, setemail] = useState("");
  const [password, setpassword] = useState("");
  const [loginFail, setloginFail] = useState(false);

  // const FacbookUrl = `${fbauthorizeURL}?client_id=${fbappID}&redirect_uri=${fbredirectUrl}&state={"abc"}&response_type=token`;
  const handlelogin = (event) => {
    event.preventDefault();
    // var formdata = new FormData();
    // formdata.append("email", email);
    // formdata.append("password", password);

    signin({ email, password })
      .then((res) => {
        console.log("res", res);
        localStorage.setItem("token", res.data.token);
        localStorage.setItem("isLogin", true);
        dispatch(loginSuccess(res.data.token));
        history.push("/gallaries");
      })
      .catch((err) => {
        console.log("err", err);
        setloginFail(true);
        setTimeout(() => {
          setloginFail(false);
        }, 5000);
      });
  };

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={12} md={12} className={classes.image}>
        <Grid
          item
          xs={false}
          sm={12}
          md={12}
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
            marginTop: 160,
          }}
        >
          <Grid
            item
            xs={12}
            sm={6}
            md={4}
            component={Paper}
            square
            // style={{ transform: "rotate(-15deg)" }}
          >
            <div className={classes.paper}>
              <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Sign in
              </Typography>
              <form
                className={classes.form}
                noValidate
                onSubmit={(e) => handlelogin(e)}
              >
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  autoFocus
                  onChange={(e) => setemail(e.target.value)}
                />
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  onChange={(e) => setpassword(e.target.value)}
                />
                <FormControlLabel
                  control={<Checkbox value="remember" color="primary" />}
                  label="Remember me"
                />
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                  // onClick={(e) => handlelogin(e)}
                >
                  Sign In
                </Button>
                {/* <Button
                  fullWidth
                  variant="contained"
                  style={{ backgroundColor: "#3b5998", color: "#fff" }}
                  className={classes.submit}
                  onClick={() => window.open(FacbookUrl)}
                >
                  <FacebookIcon style={{ marginRight: 12 }} />
                  Sign in with Facebook
                </Button> */}

                {/* <Button
                  fullWidth
                  variant="contained"
                  style={{ backgroundColor: "red", color: "#fff" }}
                  className={classes.submit}
                  onClick={() => {
                    window.open(
                      "https://nowornever2.chickenkiller.com" +
                        "/oauth/google/connect"
                    );
                    // signinGoogle().then((res) => {
                    //   console.log("res", res);
                    // });
                  }}
                >
                   <FacebookIcon style={{ marginRight: 12 }} /> 
                  Sign in with Google
                </Button> 
                */}

                <Grid container>
                  {/* <Grid item xs>
                    <Link href="#" variant="body2">
                      Forgot password?
                    </Link>
                  </Grid> */}
                  <Grid item>
                    <Link href="/signup" variant="body2">
                      {"Don't have an account? Sign Up"}
                    </Link>
                  </Grid>
                </Grid>
                <Box mt={5}>
                  <Copyright />
                </Box>
              </form>
            </div>
          </Grid>
        </Grid>
      </Grid>
      {loginFail ? (
        <Collapse in={loginFail}>
          <Alert
            severity="error"
            style={{
              position: "fixed",
              bottom: 12,
              right: 12,
              left: "70%",
              backgroundColor: "#333",
              color: "#fff",
            }}
          >
            <AlertTitle>Failed</AlertTitle>
            Can not sign in — <strong>check it out!</strong>
          </Alert>
        </Collapse>
      ) : (
        <> </>
      )}
    </Grid>
  );
}
