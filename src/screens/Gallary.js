import React, { useEffect, useState } from "react";
import Header from "../components/Header";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import { fetchImages } from "../api/index";
import { Grid, Hidden } from "@material-ui/core";
import ImageCard from "../components/ImageCard";
import { fbappID } from "../api/oauth";
import { makeStyles } from "@material-ui/core/styles";
import ViewListIcon from "@material-ui/icons/ViewList";
import ViewModuleIcon from "@material-ui/icons/ViewModule";
import ViewQuiltIcon from "@material-ui/icons/ViewQuilt";
import ViewAgendaIcon from "@material-ui/icons/ViewAgenda";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ViewComfyIcon from "@material-ui/icons/ViewComfy";
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: 500,
    height: 450,
  },
}));
const Gallary = (props) => {
  const [imgList, setimgList] = useState([]);
  const token = useSelector((state) => state.auth.token);
  let { id } = useParams();
  const classes = useStyles();
  const [num, setnum] = useState(3);

  // console.log("props.location.state.gallaryId", props.location.state.gallaryId);

  useEffect(() => {
    fetchImg();
  }, []);

  const [view, setView] = React.useState("list");

  const handleChange = (event, nextView) => {
    setnum(nextView);
  };

  const fetchImg = () => {
    fetchImages(token, id)
      .then((res) => {
        console.log("res", res);
        const sorted = res.data.sort(
          (a, b) => new Date(b.CreatedAt) - new Date(a.CreatedAt)
        );
        setimgList(sorted);
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  return (
    <div>
      <Header title="Photo Gallary" />
      <div style={styles.container}>
        {/* <div className={classes.root}> */}
        <Hidden smDown>
          <ToggleButtonGroup
            value={num}
            exclusive
            onChange={handleChange}
            style={{ marginBottom: 36, marginLeft: "auto" }}
          >
            <ToggleButton value="12" aria-label="list">
              <ViewAgendaIcon />
            </ToggleButton>
            <ToggleButton value="4" aria-label="module">
              <ViewModuleIcon />
            </ToggleButton>
            <ToggleButton value="3" aria-label="quilt">
              <ViewComfyIcon />
            </ToggleButton>
          </ToggleButtonGroup>
        </Hidden>

        <Grid container spacing={3}>
          {imgList.map((item, index) => {
            return (
              <>
                <ImageCard
                  key={index}
                  item={item}
                  index={index}
                  token={token}
                  onClick={() => {}}
                  onFavClick={() => {
                    alert("fav");
                  }}
                  onShareClick={() => {
                    window.open("https://www.facebook.com/sharer/sharer.php");
                  }}
                  shareButton={false}
                  favButton={false}
                  settingButton={false}
                  deleteButton={false}
                  bigPicture={true}
                  numRow={num}
                />
              </>
            );
          })}
        </Grid>
      </div>
    </div>
  );
};

export default Gallary;

const styles = {
  container: {
    display: "flex",
    flexDirection: "column",
    padding: "0px 56px 24px 56px",
    justifyContent: "flex-end",
  },
};
