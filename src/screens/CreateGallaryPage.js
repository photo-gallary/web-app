import React, { useState, useEffect } from "react";
import Header from "../components/Header";
import {
  Card,
  CardContent,
  Typography,
  Grid,
  Fade,
  Backdrop,
  Modal,
  TextField,
  Button,
  Collapse,
} from "@material-ui/core";

import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import {
  fetchGallary,
  addGallary,
  deleteGallary,
  getSession,
} from "../api/index";
import AlbumCard from "../components/AlbumCard";
import { useSelector } from "react-redux";
import { Alert, AlertTitle } from "@material-ui/lab";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
    // maxHeight: 366,
    minWidth: 290,
    minHeight: 320,
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  avatar: {
    backgroundColor: "red",
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(6, 36, 8),
    outline: "unset",
  },
  button: {
    height: 56,
  },
}));

const CreateGallaryPage = () => {
  const history = useHistory();
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [name, setname] = useState("");
  const [gallaryList, setgallaryList] = useState([]);
  const token = useSelector((state) => state.auth.token);
  const [favState, setfavState] = useState(0);
  const [previewImg, setpreviewImg] = useState(
    "https://source.unsplash.com/random"
  );
  const [success, setsuccess] = useState(false);
  const [delSuccess, setdelSuccess] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    fetchList(token);
  }, [favState]);

  const handleAdd = () => {
    if (name === "") {
      return;
    }

    getSession(token).then((res) => {
      console.log("res", res);
      addGallary(res.data.token, name, previewImg, res.data.ID).then(() => {
        fetchList(res.data.token);
        handleClose();
        setsuccess(true);
        setTimeout(() => {
          setsuccess(false);
        }, 5000);
      });
      setname("");
    });
  };

  const handleDelete = (item) => {
    console.log("id", item);
    deleteGallary(token, item.ID).then(() => {
      fetchList();
      setdelSuccess(true);
      setTimeout(() => {
        setdelSuccess(false);
      }, 5000);
    });
  };

  const fetchList = () => {
    getSession(token).then((res) => {
      fetchGallary(res.data.token, res.data.ID)
        .then((res) => {
          console.log("res", res.data);
          setgallaryList(res.data);
        })
        .catch((err) => {
          console.log("err", err);
        });
    });
  };

  return (
    <div>
      <Header title="Gallary Management" />
      <div style={styles.container}>
        <Grid container spacing={3}>
          <Grid item md={3}>
            <Card
              className={classes.root}
              style={styles.addCard}
              onClick={handleOpen}
            >
              <CardContent>
                <Typography
                  style={{
                    textAlign: "center",
                    fontSize: 24,
                    fontWeight: "bold",
                    paddingTop: 130,
                  }}
                >
                  + New gallary
                </Typography>
              </CardContent>
            </Card>
            <Modal
              aria-labelledby="transition-modal-title"
              aria-describedby="transition-modal-description"
              className={classes.modal}
              open={open}
              onClose={handleClose}
              closeAfterTransition
              disableAutoFocus
              BackdropComponent={Backdrop}
              BackdropProps={{
                timeout: 500,
              }}
            >
              <Fade in={open}>
                <div className={classes.paper}>
                  <h2 id="transition-modal-title">Create new gallary</h2>
                  <TextField
                    id="outlined-basic"
                    label="Preview image URL"
                    variant="outlined"
                    placeholder="http://example.com/image"
                    style={{ width: "100%", margin: "12px 0px" }}
                    onChange={(e) => setpreviewImg(e.target.value)}
                  />
                  <Grid container>
                    <TextField
                      id="outlined-basic"
                      label="Gallary name"
                      variant="outlined"
                      onChange={(e) => setname(e.target.value)}
                    />
                    <Button
                      variant="outlined"
                      color="primary"
                      className={classes.button}
                      onClick={handleAdd}
                    >
                      <Typography>Add</Typography>
                    </Button>
                  </Grid>
                </div>
              </Fade>
            </Modal>
          </Grid>
          {gallaryList.map((item, index) => {
            return item.favourite === true ? (
              <AlbumCard
                item={item}
                index={index}
                token={token}
                onDeleteClick={() => {
                  if (
                    window.confirm(
                      "Are you sure you want to delete this gallary?"
                    )
                  )
                    handleDelete(item);
                }}
                onClickPic={() => {
                  history.push({
                    pathname: "/gallaries/edit/" + item.ID,
                    state: { gallaryId: item.ID },
                  });
                }}
                onShareClick={() => {
                  alert("Share");
                }}
                onFavClick={() => setfavState(favState + 1)}
              />
            ) : (
              <></>
            );
          })}
          {gallaryList.map((item, index) => {
            return item.favourite === false ? (
              <AlbumCard
                item={item}
                index={index}
                token={token}
                onDeleteClick={() => {
                  if (
                    window.confirm(
                      "Are you sure you want to delete this gallary?"
                    )
                  )
                    handleDelete(item);
                }}
                onClickPic={() => {
                  history.push({
                    pathname: "/gallaries/edit/" + item.ID,
                    state: { gallaryId: item.ID },
                  });
                }}
                onShareClick={() => {
                  alert("Share");
                }}
                onFavClick={() => setfavState(favState + 1)}
              />
            ) : (
              <></>
            );
          })}
        </Grid>
      </div>
      {success ? (
        <Collapse in={success}>
          <Alert
            severity="success"
            style={{
              position: "fixed",
              bottom: 12,
              right: 12,
              left: "70%",
              backgroundColor: "#333",
              color: "#fff",
            }}
          >
            <AlertTitle>Success</AlertTitle>
            Successfully add new gallary — <strong>check it out!</strong>
          </Alert>
        </Collapse>
      ) : (
        <> </>
      )}
      {delSuccess ? (
        <Collapse in={delSuccess}>
          <Alert
            severity="error"
            style={{
              position: "fixed",
              bottom: 12,
              right: 12,
              left: "70%",
              backgroundColor: "#333",
              color: "#fff",
            }}
          >
            <AlertTitle>Deleted</AlertTitle>
            Successfully deleted gallary — <strong>check it out!</strong>
          </Alert>
        </Collapse>
      ) : (
        <> </>
      )}
    </div>
  );
};

export default CreateGallaryPage;

const styles = {
  container: {
    flex: 1,
    padding: "24px 56px",
  },
  addCard: {
    border: "2px dashed black",
    opacity: 0.6,
    cursor: "pointer",
  },
  addedCard: {
    // height: 240,
  },
  gallaryText: { textAlign: "center", fontSize: 24, fontWeight: "bold" },
};
