import React, { useState, useEffect } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import FacebookIcon from "@material-ui/icons/Facebook";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { signin } from "../api/index";
import { useDispatch } from "react-redux";
import { loginSuccess } from "../redux/action/AuthAction";

import { useParams, useLocation, useHistory } from "react-router-dom";
import {
  fbauthorizeToken,
  fbredirectUrl,
  fbsecret,
  fbappID,
} from "../api/oauth";
function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        My Gallary
      </Link>
      {new Date().getFullYear()}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
  },
  image: {
    backgroundImage: "url(https://source.unsplash.com/random)",
    backgroundRepeat: "no-repeat",
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[50]
        : theme.palette.grey[900],
    backgroundSize: "cover",
    backgroundPosition: "center",
  },
  paper: {
    margin: theme.spacing(2, 8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));
const Callback = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();

  const useQuery = () => {
    const url = window.location.hash;
    console.log("url", url);
    return new URLSearchParams(url.slice(1));
  };

  const query = useQuery();

  const fburl = `${fbauthorizeToken}?client_id=${fbappID}&redirect_uri=${fbredirectUrl}&client_secret=${fbsecret}&code=${query.get(
    "code"
  )}`;

  useEffect(() => {
    fetch(fburl)
      .then((data) => {
        console.log("data", data);
      })
      .catch((err) => {
        console.log("err", err);
      });
  }, []);
  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={12} md={12} className={classes.image}>
        <Grid
          item
          xs={false}
          sm={12}
          md={12}
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
            marginTop: 160,
          }}
        >
          <Grid
            item
            xs={12}
            sm={6}
            md={4}
            component={Paper}
            square
            // style={{ transform: "rotate(-15deg)" }}
          >
            <div className={classes.paper}>
              <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Callback
              </Typography>
              <div> {query.get("access_token")}</div>;
            </div>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Callback;
