import React, { useState, useEffect } from "react";
import {
  Card,
  Typography,
  Grid,
  CardHeader,
  CardMedia,
  IconButton,
  CardActions,
  CardContent,
} from "@material-ui/core";
import { getUserById } from "../api/index";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ShareIcon from "@material-ui/icons/Share";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    // maxWidth: 345,
    minWidth: 230,
    minHeight: 330,
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  container: {
    backgroungColor: "red",
    padding: "24px 112px",
    height: "72opx",
    textAlign: "center",
  },
}));

const ShowCard = ({
  item,
  index,
  onFavClick,
  onShareClick,
  favButton,
  shareButton,
}) => {
  const classes = useStyles();
  const history = useHistory();
  const [name, setname] = useState("");

  useEffect(() => {
    getUserById(item.user_id).then((res) => {
      setname("By " + res.data[0].firstname + " " + res.data[0].lastname);
    });
  }, []);

  return (
    <Grid item md={4} key={index}>
      <Card
        className={classes.root}
        onClick={() =>
          history.push({
            pathname: "/gallary/" + item.ID,
            state: { gallaryId: item.ID },
          })
        }
      >
        <CardHeader
          title={item.name}
          subheader={new Date(item.CreatedAt).toLocaleDateString("en-GB")}
        />

        <CardMedia
          className={classes.media}
          image={item.preview}
          title={item.name}
        />
        <CardContent>
          <Typography>{name}</Typography>
        </CardContent>

        <CardActions disableSpacing>
          {favButton ? (
            <IconButton aria-label="add to favorites" onClick={onFavClick}>
              <FavoriteIcon />
            </IconButton>
          ) : null}
          {shareButton ? (
            <IconButton aria-label="share" onClick={onShareClick}>
              <ShareIcon />
            </IconButton>
          ) : null}
        </CardActions>
      </Card>
    </Grid>
  );
};

export default ShowCard;
