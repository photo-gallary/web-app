import React, { useState, useEffect } from "react";
import {
  Card,
  Grid,
  CardMedia,
  IconButton,
  CardActions,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  CardHeader,
  Button,
  Typography,
} from "@material-ui/core";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ShareIcon from "@material-ui/icons/Share";
import DeleteIcon from "@material-ui/icons/Delete";
import SettingsIcon from "@material-ui/icons/Settings";
import { updatePublish } from "../api/index";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import BigPicture from "react-bigpicture";

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 230,
    minHeight: 180,
  },
  media: {
    minHeight: 280,
    paddingTop: "56.25%", // 16:9
  },

  avatar: {
    backgroundColor: "red",
  },
}));
const ImageCard = ({
  item,
  index,
  onDeleteClick,
  token,
  onClick,
  onFavClick,
  onShareClick,
  shareButton,
  favButton,
  settingButton,
  deleteButton,
  bigPicture,
  deleteMode,
  path,
  numRow,
}) => {
  const classes = useStyles();
  const [hover, sethover] = useState(false);
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <Grid item md={numRow || 3} key={index}>
      <Card className={classes.root}>
        <CardHeader
          action={
            deleteButton ? (
              <IconButton
                aria-label="delete current image"
                onClick={onDeleteClick}
                style={{ cursor: "pointer" }}
              >
                <DeleteIcon />
              </IconButton>
            ) : (
              <> </>
            )
          }
          style={{ color: "#000" }}
          title={item.caption}
          subheader={new Date(item.CreatedAt).toLocaleString("en-GB")}
          onMouseOver={() => sethover(true)}
          onMouseLeave={() =>
            setTimeout(() => {
              sethover(false);
            }, 3000)
          }
        />
        {bigPicture ? (
          <BigPicture type="image" caption={item.caption} src={item.src}>
            <img
              src={item.src}
              style={
                numRow === "12"
                  ? { width: "100vw" }
                  : {
                      width: "100%",
                    }
              }
            />
          </BigPicture>
        ) : (
          <CardMedia
            className={classes.media}
            image={item.src}
            title={item.file_name}
            onMouseOver={() => sethover(true)}
            onMouseLeave={() => sethover(false)}
            style={hover ? styles.imgHover : styles.imgNon}
          >
            {settingButton ? (
              <IconButton
                aria-label="setting"
                onClick={onClick}
                style={hover ? styles.settingHover : styles.settingButton}
              >
                <SettingsIcon
                  style={{ fontSize: 48, boxShadow: "10px 0dpx 0px #000" }}
                />
              </IconButton>
            ) : (
              <></>
            )}
            {deleteMode ? (
              <>
                <IconButton
                  aria-label="delete"
                  onClick={handleClickOpen}
                  style={hover ? styles.settingHover : styles.settingButton}
                >
                  <DeleteIcon
                    style={{ fontSize: 48, boxShadow: "10px 0dpx 0px #000" }}
                  />
                </IconButton>

                <Dialog
                  open={open}
                  keepMounted
                  onClose={handleClose}
                  aria-labelledby="alert-dialog-slide-title"
                  aria-describedby="alert-dialog-slide-description"
                >
                  <DialogTitle id="alert-dialog-slide-title">
                    {"Are you sure?"}
                  </DialogTitle>
                  <DialogContent>
                    <DialogContentText id="alert-dialog-slide-description">
                      Are you sure you want to remove this image from your Photo
                      Gallary?
                    </DialogContentText>
                  </DialogContent>
                  <DialogActions>
                    <Button onClick={handleClose} color="primary">
                      Cancel
                    </Button>
                    <Button
                      onClick={() => {
                        onDeleteClick();
                        handleClose();
                      }}
                      color="primary"
                    >
                      Delete
                    </Button>
                  </DialogActions>
                </Dialog>
              </>
            ) : (
              <></>
            )}
          </CardMedia>
        )}
        <CardActions disableSpacing>
          {path ? <Typography>{item.image}</Typography> : null}
          {favButton ? (
            <IconButton aria-label="add to favorites" onClick={onFavClick}>
              <FavoriteIcon />
            </IconButton>
          ) : (
            <></>
          )}
          {shareButton ? (
            <IconButton
              aria-label="share"
              onClick={onShareClick}
              style={{ marginLeft: "auto" }}
            >
              <ShareIcon />
            </IconButton>
          ) : (
            <></>
          )}
        </CardActions>
      </Card>
    </Grid>
  );
};

export default ImageCard;

const styles = {
  settingButton: {
    display: "flex",
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: "-20%",
    color: "rgba(0, 0, 0, 0.54)",
    backgroundColor: "#bbb",
    borderRadius: "50%",
    opacity: 0.8,
    transition: "ease 1s",
  },

  settingHover: {
    // position: "absolute",
    // top: -250,
    // left: -250,
    display: "flex",
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: "-20%",
    color: "#fff",
    opacity: 0.8,
    transition: "ease 0.6s",
  },
  imgNon: {
    opacity: 1,
    transform: "scale(1.7)",
    transition: "transform .4s",
  },
  imgHover: {
    opacity: 0.8,
    cursor: "pointer",
    transform: "scale(0.9)",
    // minWidth: "100%",
    // minHeight: "100%",
    transition: "transform .4s",
  },
};
