import React, { useState, useEffect } from "react";
import {
  Card,
  Typography,
  Grid,
  CardHeader,
  CardMedia,
  Avatar,
  IconButton,
  CardActions,
  Switch,
  FormControlLabel,
  Collapse,
  FormControl,
  Input,
  InputAdornment,
  Tooltip,
} from "@material-ui/core";
import FavoriteIcon from "@material-ui/icons/Favorite";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import ShareIcon from "@material-ui/icons/Share";
import DeleteIcon from "@material-ui/icons/Delete";
import SettingsIcon from "@material-ui/icons/Settings";
import EditIcon from "@material-ui/icons/Edit";
import DoneIcon from "@material-ui/icons/Done";
// import StarIcon from "@material-ui/icons/Star";
// import StarBorderIcon from "@material-ui/icons/StarBorder";
import { updatePublish, updateFavourite, updateName } from "../api/index";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { useSelector } from "react-redux";
import { Alert, AlertTitle } from "@material-ui/lab";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
    minWidth: 230,
    minHeight: 320,
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },

  avatar: {
    backgroundColor: "red",
  },
}));

const IOSSwitch = withStyles((theme) => ({
  root: {
    width: 42,
    height: 28,
    padding: 2,
    margin: theme.spacing(2),
  },
  switchBase: {
    padding: 2,
    // marginTop: 1,

    "&$checked": {
      transform: "translateX(16px)",
      color: theme.palette.common.white,
      "& + $track": {
        backgroundColor: "#52d869",
        opacity: 1,
        border: "none",
      },
    },
    "&$focusVisible $thumb": {
      color: "#52d869",
      border: "6px solid #fff",
    },
  },
  thumb: {
    width: 24,
    height: 24,
  },
  track: {
    borderRadius: 26 / 2,
    // border: `1px solid ${theme.palette.grey[400]}`,
    backgroundColor: "#D3D3D3",
    opacity: 1,
    transition: theme.transitions.create(["background-color", "border"]),
  },
  checked: {},
  focusVisible: {},
}))(({ classes, ...props }) => {
  return (
    <Switch
      focusVisibleClassName={classes.focusVisible}
      disableRipple
      classes={{
        root: classes.root,
        switchBase: classes.switchBase,
        thumb: classes.thumb,
        track: classes.track,
        checked: classes.checked,
      }}
      {...props}
    />
  );
});

const AlbumCard = ({
  item,
  index,
  onDeleteClick,
  token,
  onClickPic,
  onFavClick,
  onShareClick,
}) => {
  const classes = useStyles();
  const [checkedB, setcheckedB] = useState(false);
  const [fav, setfav] = useState(false);
  const [hover, sethover] = useState(false);
  const [pvSuccess, setpvSuccess] = useState(false);
  const [updateSuccess, setupdateSuccess] = useState(false);
  const [favSuccess, setfavSuccess] = useState(false);
  const [edit, setedit] = useState(false);
  const [newName, setnewName] = useState("");
  useEffect(() => {
    setcheckedB(item.Status);
    setfav(item.favourite);
    setnewName(item.name);
  }, []);

  const handleChange = (id) => {
    updatePublish(token, id, !checkedB).then((res) => {
      console.log("res", res);
      setcheckedB(!checkedB);
      setpvSuccess(true);
      setTimeout(() => {
        setpvSuccess(false);
      }, 5000);
    });
  };

  const handleUpdateName = (id) => {
    updateName(id, newName).then((res) => {
      console.log("res", res);
      setedit(!edit);
      setupdateSuccess(true);

      setTimeout(() => {
        setupdateSuccess(false);
      }, 5000);
    });
  };

  const handleChangeFav = (id) => {
    updateFavourite(token, id, !fav).then((res) => {
      onFavClick();
      setfav(!fav);
    });
  };

  return (
    <Grid item md={3} key={index}>
      <Card className={classes.root}>
        <CardHeader
          // avatar={
          // <Avatar aria-label="recipe" className={classes.avatar}>
          //   O
          // </Avatar>
          // }
          action={
            <>
              {edit ? (
                <IconButton
                  aria-label="delete gallary"
                  onClick={() => handleUpdateName(item.ID)}
                >
                  <DoneIcon />
                </IconButton>
              ) : (
                <Tooltip title="Edit gallary name" placement="top">
                  <IconButton
                    aria-label="delete gallary"
                    onClick={() => setedit(!edit)}
                  >
                    <EditIcon />
                  </IconButton>
                </Tooltip>
              )}
              <Tooltip title="Delete" placement="top">
                <IconButton aria-label="delete gallary" onClick={onDeleteClick}>
                  <DeleteIcon />
                </IconButton>
              </Tooltip>
            </>
          }
          title={
            edit ? (
              <FormControl>
                <Input
                  id="standard-adornment-password"
                  value={newName}
                  onChange={(e) => setnewName(e.target.value)}
                />
              </FormControl>
            ) : (
              <Typography>{newName}</Typography>
            )
          }
          subheader={new Date(item.UpdatedAt).toLocaleString("en-GB")}
        />

        <CardMedia
          className={classes.media}
          image={item.preview}
          title={item.name}
          onMouseOver={() => sethover(true)}
          onMouseLeave={() => sethover(false)}
          onClick={onClickPic}
          style={hover ? styles.imgHover : styles.imgNon}
        >
          <IconButton
            aria-label="setting"
            style={hover ? styles.settingHover : styles.settingButton}
          >
            <SettingsIcon
              style={{ fontSize: 48, boxShadow: "10px 0dpx 0px #000" }}
            />
          </IconButton>
        </CardMedia>

        <CardActions disableSpacing>
          <Tooltip title="Favourite" placement="Bottom">
            <IconButton
              aria-label="add to favorites"
              onClick={() => handleChangeFav(item.ID)}
            >
              {fav ? (
                <FavoriteIcon style={{ color: "red" }} />
              ) : (
                <FavoriteBorderIcon />
              )}
            </IconButton>
          </Tooltip>
          {/* <Tooltip title="Share" placement="bottom">
            <IconButton aria-label="share" onClick={onShareClick}>
              <ShareIcon />
            </IconButton>
          </Tooltip> */}

          <Typography style={{ marginLeft: 36 }}>Private</Typography>
          <FormControlLabel
            control={
              <IOSSwitch
                checked={checkedB}
                onChange={() => handleChange(item.ID)}
                name="checkedB"
              />
            }
            style={{ margin: 0, padding: 0 }}
          />
          <Typography>Publish</Typography>
        </CardActions>
      </Card>

      {pvSuccess ? (
        <Collapse in={pvSuccess}>
          <Alert
            severity="success"
            style={{
              position: "fixed",
              bottom: 12,
              right: 12,
              left: "70%",
              backgroundColor: "#333",
              color: "#fff",
            }}
          >
            <AlertTitle>Success</AlertTitle>
            Successfully set privacy to gallary — <strong>check it out!</strong>
          </Alert>
        </Collapse>
      ) : (
        <> </>
      )}
      {updateSuccess ? (
        <Collapse in={updateSuccess}>
          <Alert
            severity="success"
            style={{
              position: "fixed",
              bottom: 12,
              right: 12,
              left: "70%",
              backgroundColor: "#333",
              color: "#fff",
            }}
          >
            <AlertTitle>Updated</AlertTitle>
            Successfully update gallary name — <strong>check it out!</strong>
          </Alert>
        </Collapse>
      ) : (
        <> </>
      )}
      {favSuccess ? (
        <Collapse in={favSuccess}>
          <Alert
            severity="success"
            style={{
              position: "fixed",
              bottom: 12,
              right: 12,
              left: "70%",
              backgroundColor: "#333",
              color: "#fff",
            }}
          >
            <AlertTitle>Favourite</AlertTitle>
            Successfully added your favourite gallary —{" "}
            <strong>check it out!</strong>
          </Alert>
        </Collapse>
      ) : (
        <> </>
      )}
    </Grid>
  );
};

export default AlbumCard;

const styles = {
  settingButton: {
    display: "flex",
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: -130,
    color: "rgba(0, 0, 0, 0.54)",
    backgroundColor: "#bbb",
    borderRadius: "50%",
    opacity: 0.8,
    transition: "ease 1s",
  },

  settingHover: {
    // position: "absolute",
    // top: -250,
    // left: -250,
    display: "flex",
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: -130,
    color: "#fff",
    opacity: 0.8,
    transition: "ease 0.6s",
  },
  imgNon: {
    opacity: 0.8,
  },
  imgHover: {
    opacity: 0.4,
    cursor: "pointer",
  },
};
