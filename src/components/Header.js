import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import {
  Toolbar,
  Avatar,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Card,
  Grid,
  Tooltip,
} from "@material-ui/core";
// import IconButton from "@material-ui/core/IconButton";
// import SearchIcon from "@material-ui/icons/Search";
import Typography from "@material-ui/core/Typography";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { logout } from "../redux/action/AuthAction";
import { getSession } from "../api";

const useStyles = makeStyles((theme) => ({
  toolbar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    padding: theme.spacing(2),
  },
  toolbarTitle: {
    flex: 1,
    fontSize: 36,
    fontWeight: "bold",
  },
  toolbarSecondary: {
    justifyContent: "space-between",
    overflowX: "auto",
  },
  toolbarLink: {
    padding: theme.spacing(1),
    flexShrink: 0,
  },
}));

export default function Header(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { title } = props;
  const history = useHistory();
  const isLogin = localStorage.getItem("isLogin");
  const token = localStorage.getItem("token");

  const [profile, setprofile] = useState({});
  const [open, setOpen] = React.useState(false);
  const [openProfile, setOpenProfile] = React.useState(false);

  useEffect(() => {
    if (token !== "null") {
      getSession(token).then((res) => {
        setprofile(res.data);
      });
    }
  }, []);

  const handleClickOpenProfile = () => {
    setOpenProfile(true);
  };

  const handleCloseProfile = () => {
    setOpenProfile(false);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleLogout = () => {
    dispatch(logout());
    localStorage.setItem("token", null);
    localStorage.setItem("isLogin", false);
    history.push("/signin");
  };

  return (
    <React.Fragment>
      <Toolbar className={classes.toolbar}>
        {window.location.pathname === "/gallaries/create" ? (
          <Button size="small" onClick={() => history.push("/gallaries")}>
            Home
          </Button>
        ) : isLogin === "true" ? (
          <Button
            size="small"
            onClick={() => history.push("/gallaries/create")}
          >
            Add Gallary
          </Button>
        ) : (
          <Button size="small" onClick={() => history.push("/gallaries")}>
            Home
          </Button>
        )}

        <Typography
          component="h2"
          variant="h5"
          // color="#fff"
          align="center"
          noWrap
          className={classes.toolbarTitle}
        >
          {title}
        </Typography>
        {/* <IconButton>
          <SearchIcon />
        </IconButton> */}
        {isLogin === "false" && token === "null" ? (
          <> </>
        ) : (
          <>
            <Tooltip title="Account details" placement="bottom">
              <Avatar
                aria-label="account"
                style={{ marginRight: 24 }}
                src={
                  profile.profile_image ||
                  "https://image.flaticon.com/icons/png/512/64/64572.png"
                }
                onClick={handleClickOpenProfile}
              />
            </Tooltip>

            <Dialog
              open={openProfile}
              onClose={handleCloseProfile}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">{"Profile"}</DialogTitle>
              <DialogContent style={{ padding: "12px 56px" }}>
                <Grid container md={12} style={{ marginBottom: 24 }}>
                  <Avatar
                    aria-label="account"
                    style={{ marginRight: 24 }}
                    src={
                      profile.profile_image ||
                      "https://image.flaticon.com/icons/png/512/64/64572.png"
                    }
                  />
                  <Typography>{profile.email}</Typography>
                </Grid>
                <Grid item md={12}>
                  <Typography>
                    <strong>First name: </strong>
                    {profile.firstname}
                  </Typography>
                  <Typography>
                    <strong>Last name: </strong>
                    {profile.lastname}
                  </Typography>
                  <Typography>
                    <strong>Created at: </strong>
                    {new Date(profile.CreatedAt).toLocaleDateString()}
                  </Typography>
                </Grid>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleCloseProfile} color="primary">
                  Close
                </Button>
              </DialogActions>
            </Dialog>
          </>
        )}
        {isLogin === "false" && token === "null" ? (
          <Button
            variant="outlined"
            size="small"
            onClick={() => {
              history.push("/signin");
            }}
          >
            Sign in
          </Button>
        ) : (
          <>
            <Button variant="outlined" size="small" onClick={handleClickOpen}>
              Sign out
            </Button>
            <Dialog
              open={open}
              keepMounted
              onClose={handleClose}
              aria-labelledby="alert-dialog-slide-title"
              aria-describedby="alert-dialog-slide-description"
            >
              <DialogTitle id="alert-dialog-slide-title">
                {"Are you sure?"}
              </DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-slide-description">
                  Are you sure you want to sign out from your Photo Gallary
                  account?
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleClose} color="primary">
                  Cancel
                </Button>
                <Button onClick={handleLogout} color="primary">
                  Sign out
                </Button>
              </DialogActions>
            </Dialog>
          </>
        )}
      </Toolbar>
      <Toolbar
        component="nav"
        variant="dense"
        className={classes.toolbarSecondary}
      ></Toolbar>
    </React.Fragment>
  );
}

Header.propTypes = {
  sections: PropTypes.array,
  title: PropTypes.string,
};
