import axios from "axios";

// export const host = "http://localhost:8080";
// export const host = process.env.REACT_APP_API_ENV;
export const host = "https://nowornever2.chickenkiller.com";

if (host === "" || host === undefined) {
  throw new Error("API is not assigned");
}

axios.interceptors.request.use(async (config) => {
  const token = await localStorage.getItem("token");
  if (token != null) {
    config.headers = { Authorization: `Bearer ${token}` };
  }

  return config;
});

//------------------------*USER*------------------------

export function getSession(token) {
  return axios.get(`${host}/sessions`);
}

export function signin(form) {
  console.log("password", form["password"]);
  return axios.post(`${host}/signin`, form);
}

export function signinGoogle() {
  return axios.get(`${host}/oauth/google/connect`);
}

export function signup({
  firstname,
  lastname,
  email,
  password,
  profile_image,
}) {
  return axios.post(`${host}/signup`, {
    firstname,
    lastname,
    email,
    password,
    profile_image,
  });
}

export function getUserById(id) {
  return axios.get(`${host}/user/${id}`);
}

//------------------------*GALLARY*------------------------

export function fetchGallary(token, id) {
  return axios.get(`${host}/gallaries/${id}`);
}

export function fetchOpenGallary() {
  return axios.get(`${host}/gallaries`);
}

export function fetchGallaryById(id, token) {
  return axios.get(`${host}/gallary/${id}`);
}

export function addGallary(token, name, preview, user_id) {
  console.log("token----------------", token);
  return axios.post(`${host}/gallaries`, {
    name,
    preview,
    user_id,
  });
}

export function deleteGallary(token, id) {
  return axios.delete(`${host}/gallary/${id}`);
}

export function updatePublish(token, id, status) {
  return axios.patch(`${host}/gallary/${id}`, {
    status,
  });
}

export function updateFavourite(token, id, favourite) {
  return axios.patch(`${host}/gallary/${id}/fav`, {
    favourite,
  });
}

export function updateName(id, name) {
  return axios.patch(`${host}/gallary/${id}/name`, {
    name,
  });
}
//------------------------*IMAGE*------------------------

export function fetchImages(token, gal_id) {
  return axios.get(`${host}/gallary/${gal_id}/images`);
}

export function addImage(token, gal_id, form) {
  return axios.post(`${host}/gallary/${gal_id}/images`, form);
}

export function deleteImage(token, id) {
  return axios.delete(`${host}/images/${id}`);
}

export function updateCaption(id, caption) {
  return axios.patch(`${host}/images/${id}`, {
    caption,
  });
}
